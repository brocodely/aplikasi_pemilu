<?php

namespace Database\Factories;

use App\Models\UserAccessMenu;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserAccessMenuFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserAccessMenu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
