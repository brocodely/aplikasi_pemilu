<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserMenu;
use App\Models\UserRole;
use App\Models\UserSubMenu;
use App\Models\UserAccessMenu;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userRole = ['Administrator', 'User'];
        $userMenu = ['Main Menu', 'Profile', 'Setting'];
        $userSubMenu = [
            [
                'menu_id' => 1,
                'name' => 'Home',
                'url' => 'home',
                'icon' => 'fas fa-home'
            ],
            [
                'menu_id' => 2,
                'name' => 'Manage Profile',
                'url' => 'profile',
                'icon' => 'far fa-user-circle'
            ],
            [
                'menu_id' => 2,
                'name' => 'Change Password',
                'url' => 'profile/change-password',
                'icon' => 'bi bi-key-fill'
            ],
            [
                'menu_id' => 3,
                'name' => 'Menu Management',
                'url' => 'menu',
                'icon' => 'bi bi-menu-button-wide'
            ],
            [
                'menu_id' => 3,
                'name' => 'Sub Menu Management',
                'url' => 'submenu',
                'icon' => 'bi bi-menu-button'
            ],
            [
                'menu_id' => 3,
                'name' => 'Role Management',
                'url' => 'role',
                'icon' => 'bi bi-person-bounding-box'
            ],
            [
                'menu_id' => 3,
                'name' => 'User Management',
                'url' => 'user',
                'icon' => 'bi bi-person-check-fill'
            ],
        ];
        $userAccessMenu = [
            [
                'role_id' => 1,
                'menu_id' => 1,
            ],
            [
                'role_id' => 1,
                'menu_id' => 2,
            ],
            [
                'role_id' => 1,
                'menu_id' => 3,
            ],
            [
                'role_id' => 2,
                'menu_id' => 1,
            ],
            [
                'role_id' => 2,
                'menu_id' => 2,
            ],
        ];

        User::factory()->create();
        // UserRole::factory()->create();

        // user role
        foreach ($userRole as $key => $role) {
            UserRole::create([
                'name' => $role
            ]);
        }

        // user menu
        $order = 0;
        foreach ($userMenu as $key => $menu) {
            $order++;
            $um = UserMenu::create([
                'name' => $menu,
                'order' => $order
            ]);

            // user sub menu
            $orderSub = 0;
            foreach ($userSubMenu as $key => $subMenu) {
                if ($um->id == $subMenu['menu_id']) {
                    $orderSub++;
                    UserSubMenu::create([
                        'menu_id' => $subMenu['menu_id'],
                        'name' => $subMenu['name'],
                        'url' => $subMenu['url'],
                        'icon' => $subMenu['icon'],
                        'is_active' => 1,
                        'order' => $orderSub
                    ]);
                }
            }
        }

        // // user sub menu
        // $order = 0;
        // foreach ($userSubMenu as $key => $subMenu){
        //     $order ++;
        //     UserSubMenu::create([
        //         'menu_id' => $subMenu['menu_id'],
        //         'name' => $subMenu['name'],
        //         'url' => $subMenu['url'],
        //         'is_active' => 1,
        //         'order' => $order
        //     ]);
        // }

        // user access menu
        foreach ($userAccessMenu as $key => $access) {
            UserAccessMenu::create([
                'role_id' => $access['role_id'],
                'menu_id' => $access['menu_id'],
            ]);
        }
    }
}
