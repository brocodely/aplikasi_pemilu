<?php

namespace App\Models;

use App\Models\UserAccessMenu;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserMenu extends Model
{

    use HasFactory;
        
    protected $fillable = ['name', 'order'];

    public function access()
    {
        return $this->hasMany(UserAccessMenu::class, 'menu_id', 'id');
    }
}
