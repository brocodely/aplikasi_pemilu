@extends('layouts.main')
@section('title', 'Home')
@section('content')
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Content-->
    <div class="content fs-6 d-flex flex-column-fluid" id="kt_content">
        <!--begin::Container-->
        <div class="container">
            {{-- disini --}}
            <div class="row">
                <div class="col-lg-6">
                    <div class="card shadow-sm">
                        <div class="card-header bg-light-primary">
                            <div class="card-title fs-1 fw-boldest text-uppercase text-center">hasil perolehan suara</div>
                        </div>
                        <div class="card-body">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card shadow-sm">
                        <div class="card-header bg-light-primary">
                            <div class="card-title fs-1 fw-boldest text-uppercase text-center">lihat suara</div>
                        </div>
                        <div class="card-body">
                            <label for="wilayah" class="form-label">Lihat Wilayah</label>
                            <select class="form-select form-select-lg" id="wilayah" data-control="select2" data-placeholder="Pilih Wilayah" data-allow-clear="true">
                                <option></option>
                                <option value="1">Kec. Bantargebang</option>
                                <option value="2">Kec. Sumur Batu</option>
                                <option value="2">Kec. Setu</option>
                                <option value="2">Kec. Cikiwul</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            {{-- /disini --}}
        </div>
        <!--end::Container-->
    </div>
    <!--end::Content-->
</div>

{{-- CHART --}}
<script>
    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Caleg 1', 'Caleg 2', 'Caleg 3', 'Caleg 4', 'Caleg 5', 'Caleg 6'],
            datasets: [{
                label: '# of Votes',
                data: [5, 9, 6, 10, 8, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
@endsection
