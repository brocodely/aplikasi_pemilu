<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row flex-stack">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted fw-bold me-2">Copyright &copy; ThreeDev | Powered by <a href="https://keenthemes.com/">Keenthemes</a></span>            
        </div>
        <!--end::Copyright-->
        <!--begin::Menu-->
        <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
            <li class="menu-item">
                <a href="" target="_blank" class="menu-link px-2">About</a>
            </li>
            <li class="menu-item">
                <a href="" target="_blank" class="menu-link px-2">Support</a>
            </li>
            <li class="menu-item">
                <a href="" target="_blank" class="menu-link px-2">Follow</a>
            </li>
        </ul>
        <!--end::Menu-->
    </div>
    <!--end::Container-->
</div>