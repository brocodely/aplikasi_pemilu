
<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Start HTML Pro - Bootstrap 5 HTML Multipurpose Admin Dashboard Template 
Purchase: https://keenthemes.com/products/start-html-pro
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>Register | Dynasty</title>
		<meta name="description" content="Website buku digital pengelola pengeluaran, solusi pintar mengatur uang" />
		<meta name="keywords" content="Dynasty Book, dynasty book, DynastyBook, dynastybook, pengelola keuangan, website pengelola keuangan, pengeluaran, money management" />
		<link rel="canonical" href="Https://preview.keenthemes.com/start" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{ URL::asset('template/theme/dist') }}/assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{ URL::asset('template/theme/dist') }}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ URL::asset('template/theme/dist') }}/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<!--Begin::Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&amp;l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-5FS8GGP');</script>
		<!--End::Google Tag Manager -->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" data-sidebar="on" class="bg-white header-fixed header-tablet-and-mobile-fixed toolbar-enabled sidebar-enabled">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="d-flex flex-column flex-lg-row flex-column-fluid" id="kt_login">
				<!--begin::Aside-->
				<div class="d-flex flex-column flex-lg-row-auto bg-primary w-lg-600px pt-15 pt-lg-0">
					<!--begin::Aside Top-->
					<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15 text-center">
						<!--begin::Aside Logo-->
						<a href="{{ URL::asset('template/theme/dist') }}/index.html" class="mb-6">
							<img alt="Logo" src="{{ URL::asset('template/theme/dist') }}/assets/media/logos/logo-default.svg" class="h-75px" />
						</a>
						<!--end::Aside Logo-->
						<!--begin::Aside Subtitle-->
						<h3 class="fw-bolder fs-2x text-white lh-lg">Solusi cerdas penghitung <br>pengeluaran anda</h3>
						<!--end::Aside Subtitle-->
					</div>
					<!--end::Aside Top-->
					<!--begin::Aside Bottom-->
					<div class="d-flex flex-row-fluid bgi-size-contain bgi-no-repeat bgi-position-y-bottom bgi-position-x-center min-h-350px" style="background-image: url({{ URL::asset('template/theme/dist') }}/assets/media/illustrations/winner.png)"></div>
					<!--end::Aside Bottom-->
				</div>
				<!--begin::Aside-->
				<!--begin::Content-->
				<div class="login-content flex-lg-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden py-20 px-10 p-lg-7 mx-auto mw-450px w-100">
					<!--begin::Wrapper-->
					<div class="d-flex flex-column-fluid flex-center py-10">
						<!--begin::Signin Form-->
						<form class="form w-100" method="POST" action="{{ route('register') }}">
                            @csrf
							
							<div class="pb-5 pb-lg-15">
								<h3 class="fw-bolder text-dark display-6">Sign Up</h3>
								<p class="text-muted fw-bold fs-3">Daftarkan diri anda sekarang!</p>
							</div>
							<!--end::Title-->
							<!--begin::Form group-->
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="name">Nama Lengkap</label>
								<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Masukan nama lengkap anda" >

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="email">Email</label>
								<input id="kt_inputmask_8" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Masukan alamat email anda">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="phone">No. Telp</label>
								<input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" placeholder="Masukan no telp anda">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="gender">Gender</label>								
								<select id="gender" class="form-select @error('gender') is-invalid @enderror" aria-label="Gender" name="gender" value="{{ old('gender') }}" required autocomplete="gender">
									<option value="">Pilih jenis kelamin</option>
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="address">Alamat</label>
								<input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" placeholder="Masukan alamat anda">

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>							
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="fv-row mb-5">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="password">Password</label>
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Masukan password anda">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="fv-row mb-10">
								<label class="form-label fs-6 fw-bolder text-dark pt-5" for="password-confirm" >Confirm Password</label>
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Masukan ulang password anda">
							</div>
							<!--end::Form group-->
							<!--begin::Form group-->
							
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="d-flex flex-wrap pb-lg-0 pb-5">
								<button type="submit" id="kt_login_signup_form_submit_button" class="btn btn-primary fw-bolder fs-6 px-8 py-4 my-3 me-4">Submit</button>
								<button type="button" id="kt_login_signup_form_cancel_button" class="btn btn-light-primary fw-bolder fs-6 px-8 py-4 my-3" onclick="history.back()">Cancel</button>
							</div>
							<!--end::Action-->
						</form>
						<!--end::Signin Form-->
					
					</div>
					<!--end::Wrapper-->
					<!--begin::Footer-->
					<div class="d-flex justify-content-lg-start justify-content-center align-items-center py-7 py-lg-0">
						<a href="#" class="text-primary fw-bolder fs-4">Terms</a>
						<a href="#" class="text-primary ms-10 fw-bolder fs-4">Plans</a>
						<a href="#" class="text-primary ms-10 fw-bolder fs-4">Contact Us</a>
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{ URL::asset('template/theme/dist') }}/assets/plugins/global/plugins.bundle.js"></script>
		<script src="{{ URL::asset('template/theme/dist') }}/assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="{{ URL::asset('template/theme/dist') }}/assets/js/custom/general/login.js"></script>
		<script src="{{ URL::asset('template/theme/dist') }}/assets/js/custom/apps/chat/chat.js"></script>
		<script src="{{ URL::asset('template/theme/dist') }}/assets/js/custom/intro.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
		<!--Begin::Google Tag Manager (noscript) -->
		<noscript>
			<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<!--End::Google Tag Manager (noscript) -->
		<script>
			// Email address
			Inputmask({
				mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
				greedy: false,
				onBeforePaste: function (pastedValue, opts) {
					pastedValue = pastedValue.toLowerCase();
					return pastedValue.replace("mailto:", "");
				},
				definitions: {
					"*": {
						validator: '[0-9A-Za-z!#$%&"*+/=?^_`{|}~\-]',
						cardinality: 1,
						casing: "lower"
					}
				}
			}).mask("#kt_inputmask_8");
		</script>
	</body>
	<!--end::Body-->
</html>